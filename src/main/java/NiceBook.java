package com.greenform.scrapers;

import java.io.File;
class NiceBook {
  public static void main(String[] args) {
	NiceBook.createWorkingDirectory();
	NiceBook.createWorkingSubDirectory(args[0]);
  }
  private static void createWorkingDirectory() {
	Out.begin("Create the working directory");
    String path = FileSystemLookup.getWorkingDirectoryPath();
    new File(path).mkdirs();
	Out.end();
  }
  private static void createWorkingSubDirectory(String name) {
	Out.begin("Create the working subdirectory (" + name + ")");
	Out.end();
  }
}

